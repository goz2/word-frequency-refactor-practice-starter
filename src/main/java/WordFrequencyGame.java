import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String REGEX = "\\s+";
    public static final String ERROR_MESSAGE = "Calculate Error";

    public String getResult(String inputStr) {
        try {
            List<WordFrequency> wordFrequencyList = convertInputStringToWordFrequencyList(inputStr);

            Map<String, List<WordFrequency>> wordGroupMap = groupInputByWord(wordFrequencyList);

            List<WordFrequency> wordFrequencyListWithCount = convertInputMapToList(wordGroupMap);

            List<WordFrequency> wordFrequencySortedList = sortWordFrequencyList(new ArrayList<>(wordFrequencyListWithCount));

            return formatWordFrequencyResult(wordFrequencySortedList);
        } catch (Exception e) {
            return ERROR_MESSAGE;
        }
    }

    private static List<WordFrequency> sortWordFrequencyList(List<WordFrequency> wordFrequencyList) {
        wordFrequencyList.sort((firstWord, secondWord) -> secondWord.getWordCount() - firstWord.getWordCount());
        return wordFrequencyList;
    }

    private static List<WordFrequency> convertInputMapToList(Map<String, List<WordFrequency>> wordFrequencyMap) {
        return wordFrequencyMap.keySet().stream().map(key -> new WordFrequency(key, wordFrequencyMap.get(key).size())).collect(Collectors.toList());
    }

    private static String formatWordFrequencyResult(List<WordFrequency> wordFrequencyList) {

        return wordFrequencyList.stream()
                .map(wordFrequency -> String.format("%s %s", wordFrequency.getValue(), wordFrequency.getWordCount()))
                .collect(Collectors.joining("\n"));
    }

    private static List<WordFrequency> convertInputStringToWordFrequencyList(String inputStr) {
        return Arrays.stream(inputStr.split(REGEX))
                .map(word -> new WordFrequency(word, 1))
                .collect(Collectors.toList());
    }

    private Map<String, List<WordFrequency>> groupInputByWord(List<WordFrequency> wordFrequencyList) {
        Map<String, List<WordFrequency>> wordGroupMap = new HashMap<>();
        wordFrequencyList.forEach(wordFrequency -> wordGroupMap.computeIfAbsent(wordFrequency.getValue(), k -> new ArrayList<>()).add(wordFrequency));
        return wordGroupMap;
    }
}
